#/bin/bash
base=`basename $1`
file=`echo ${base%.*}`
echo $file
pandoc -f markdown $1 -o /tmp/$file.tei
xalan -xsl tei2parcours.xsl -in /tmp/$file.tei -out out/$file.prog
rm /tmp/$file.tei
