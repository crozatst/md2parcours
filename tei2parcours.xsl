<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:par="sc.soft:parcours" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    
    <xsl:output 
        method="xml" 
        indent="yes" 
    />

    <xsl:param name="module-type-id">id:5P5YC3ZiYLHQIrXetwoAIz</xsl:param>
    <xsl:param name="module-depot-base">depot:/modules/</xsl:param>
    
    <xsl:template match="div[@type='level1']">
        <sc:item>
            <par:prog xml:id="{generate-id()}">
                <par:progM>
                    <sp:title>
                        <par:rTitle>
                            <sc:para>
                                <xsl:value-of select="head"/>
                            </sc:para>
                        </par:rTitle>
                    </sp:title>
                </par:progM>
                <sp:info>
                    <par:bloc>
                        <par:rTitleOptM/>
                        <sp:co>
                            <par:txt>
                                <xsl:apply-templates select="p"/>
                            </par:txt>
                        </sp:co>
                    </par:bloc>
                </sp:info>
                <xsl:apply-templates select="div"/>
            </par:prog>
        </sc:item>        
    </xsl:template>
    
    <xsl:template match="p">
        <sc:para><xsl:apply-templates select="* | text()"/></sc:para>
    </xsl:template>
        
    <xsl:template match="ref">
        <sc:phrase role="url">
            <par:url>
                <sp:title><par:rTitle><sc:para><xsl:value-of select="."/></sc:para></par:rTitle></sp:title>
                <sp:remote><par:urlR><sc:location><xsl:value-of select="@target"/></sc:location></par:urlR></sp:remote>
            </par:url>
            <xsl:value-of select="."/>
        </sc:phrase>        
    </xsl:template>
    
    <xsl:template match="div[@type='level2']">
        <sp:sequence>
            <par:sequence xml:id="{generate-id()}">
                <par:sequenceM>
                    <sp:title>
                        <par:rTitle>
                            <sc:para>
                                Séquence <xsl:number/> · <xsl:value-of select="head"/>
                            </sc:para>
                        </par:rTitle>
                    </sp:title>
                </par:sequenceM>
                <sp:info>
                    <par:bloc>
                        <par:rTitleOptM/>
                        <sp:co>
                            <par:txt>
                                <xsl:apply-templates select="list"/>
                            </par:txt>
                        </sp:co>
                    </par:bloc>
                </sp:info>
            </par:sequence>
        </sp:sequence>        
    </xsl:template>
    
    <xsl:template match="list">
        <sc:itemizedList>
            <xsl:apply-templates/>
        </sc:itemizedList>
    </xsl:template>
    
    <xsl:template match="item">        
        <sc:listItem>
            <sc:para>
                <xsl:apply-templates/>
            </sc:para>
        </sc:listItem>
    </xsl:template>    
    
    <xsl:template match="item/p">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="hi[@rendition='simple:bold']">
        <!-- Type -->        
        <xsl:choose>
            <xsl:when test=".='C'">Cours · </xsl:when>
            <xsl:when test=".='A'">Atelier · </xsl:when>
            <xsl:when test=".='P'">Projet · </xsl:when>       
            <xsl:when test=".='D'">TD · </xsl:when>    
            <xsl:when test=".='R'">Rendu · </xsl:when>  
            <xsl:when test=".='CC'">Complément · </xsl:when>            
            <xsl:otherwise><xsl:value-of select="."/> · </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Link to depot -->    
    <xsl:template match="hi[@rendition='simple:italic']">
        <xsl:text> </xsl:text>
        <sc:phrase role="opMod">            
            <par:opMod xml:id="{generate-id()}">
                <sp:type sc:refUri="{$module-type-id}"/>
                <sp:remote>
                    <par:opModR>
                        <sc:location><xsl:value-of select="$module-depot-base"/><xsl:value-of select="."/></sc:location>
                    </par:opModR>
                </sp:remote>
            </par:opMod>          
        </sc:phrase>   
        <xsl:text> </xsl:text>                      
    </xsl:template>

    <xsl:template match="seg[@type='code']">
        <!-- Timing -->
        [<xsl:value-of select="."/>]
    </xsl:template>


</xsl:stylesheet>
