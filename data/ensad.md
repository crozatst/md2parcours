# Framasoft à l'ENSAD 

École des arts décoratifs · [ENSAD](https://www.ensad.fr/) · 12 décembre 2024

## Conférence
- **C** "Nos outils numériques sont politiques, comment faire des choix ?" *enjeux/redecentralisation_ensad* `1h15`

## Échanges
- **A** Quelles pratiques sembles faciles à modifier ? Qu'est-ce qui l'est moins ? `0h45`

