# Toolbox Reset!

The European network of independent cultural and media organisations · [Reset!](https://reset-network.eu/) · 27 November 2024

## Presentation
- **C** Get out of the Big Tech */enjeux/redecentralisation_en-reset/* `1h30`

## Q&A
- **A** Focus on practical aspects: what seems easy, what seems harder ? `0h30`

