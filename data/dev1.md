# Dev1 · Initiation à la programmation 

avec JavaScript et Python (dans cette version les exercices et compléments sont en JavaScript seulement) 

## Mise en place

- Introduction à JavaScript *dev/js01*`3h`
- **Option** Introduction à Python *dev/intro-python* `3h`

## Variables

- Variables et constantes *dev/js05* `3h`
- Types et portée des variables *dev/js06* `3h`

## Nombres et chaînes de caractères

- Les nombres *dev/js07* `3h`
- Les chaînes de caractères *dev/js08* `3h`

## Alternatives (if)

- Les structures alternatives (si alors) *dev/js09* `3h`
- **Méthodo** Utiliser les documentations JavaScript *dev/js19* `3h`

## Itérations (for et while)

- Les structures itératives (boucles) *dev/js10* `3h`
- **Méthodo** Gestion des bugs : stratégies et outils pour Javascript *dev/js17*`3h`

## Entrée/sortie et dates

- Les entrées et les sorties *dev/js11* `3h`
- **Complément** Algorithmes et programmes *dev/js03* `3h`

## Dates 

- Les dates *dev/js12* `3h`
- **Méthodo** Notions de test unitaire et fonctionnel *dev/js18* `3h`

## Tableaux

- Les tableaux *dev/js13* `3h`
- **Complément** Quelques algorithmes classiques *dev/js04* `3h`

## Fonctions

- Les fonctions *dev/js15* `3h`
- **Complément** Valeur, variable, référence *dev/js16* `3h`

## Compléments 

- **Complément** Les enregistrements en JavaScript (JSON) *dev/js14* `3h`
- **Complément** Paradigmes de programmation *dev/js02* `3h`

