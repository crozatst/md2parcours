# Lownum1 · Introduction à la lowtechisation

Lowtechisation et numérique, partie I 

## Introduction
- **C** Introduction : organisation du cours et aperçu des concepts *lownum/low00* `2h`
- **A** Scenarios Ademe *lownum/atelier-ademe* `1h`
- **P** Initialisation *methodo/initialisation-lownum* `3-7h`

## Problématisation 
- **C** Problématisation  : lowtechisation versus technosolutionnisme *lownum/low01* `2h`
- **A** Lifephone *lownum/atelier-lifephone*`1h`
- **P** État de l'art *methodo/etat-de-l-art* `3-7h`

## Concepts 
- **C** Concepts de la lowtechisation : valeurs, leviers, tensions *lownum/low02a* `2h`
- **A** Brainwriting *lownum/atelier-brainwriting* `1h`
- **P** Idéation *methodo/ideation* `3-7h`

## Méthodologie
- **C** Méthode générale pour lowtechiser et introduction à l'analyse fonctionnelle *lownum/low02b* `2h`
- **A** Petites histoires *lownum/atelier-maquettage* `1h`
- **P** Maquettage *methodo/maquettage* `3-7h`

## Évaluation 
- **C** Évaluation qualitative réflexive a priori *lownum/low03* `2h`
- **A** Empreinte fantôme *lownum/atelier-evaluation* `1h`
- **P** Évaluation *methodo/evaluation* `3-7h`

## Clôture 
- **C** Agir et prolonger : contribuer à la low-technicisation en tant qu'ingénieur *lownum/low21* `2h`
- **A** Emploi idéal *lownum/atelier-emploi-ideal* `1h`
- **P** Publication *methodo/publication* `3-7h`
