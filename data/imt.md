# Lownum IMT

[Institut Mines-Télécom](https://www.imt-atlantique.fr/fr) · campus de Brest · Spécialisation Transformation Numérique et Transition · 20 novembre 2024

## Initialisation
- **A** Lifephone *lownum/atelier-lifephone* `1h15`

## Introduction théorique 
- **C** La lowtechisation contre le technosolutionnisme *lownum/low01-02-imt* `1h15`

## Introduction pratique
- **A** Atelier projet *lownum/atelier-decouverte-projet* `2h30`


