# Lownum2 · Approfondissement de la lowtechisation dans le contexte du numérique

Lowtechisation et numérique, Partie II

## Impacts
- **C**  De l’optimisation logicielle au permacomputing *lownum/low06* `2h`
- **A**  Regards croisés *lownum/atelier-regards-croises* `1h`
- **P**  Regards croisés  `3-7h`

## Soin
- **C** Constitutivité technique et soin dans la technique *lownum/low11* `2h`
- **A** Carré du soin *lownum/atelier-carre-du-soin* `1h`
- **P** v2 (+ Carré du soin) *methodo/evaluation* `3-7h`

## Besoins
- **C** Besoins artificiels, décroissance, fermeture *lownum/low12* `2h`
- **A** Ataraxie *lownum/atelier-besoins* `1h`
- **P** v3 (+ Ataraxie & Décroissance) *methodo/evaluation* `3-7h`

## Communs
- **C** Communs, culture libre et engagements collectifs *lownum/low14* `2h`
- **A** Communs *lownum/atelier-communs* `1h`
- **P** v4 (+ Communs & Fermeture) *methodo/evaluation* `3-7h`

## Droit
- **C** Outils et enjeux juridiques et politiques *lownum/low16* `2h`
- **A** Diamant *lownum/atelier-diamant* `1h`
- **P** v5 (+ Diamant & Sed lex) *methodo/evaluation* `3-7h`

## Ingénierie
- **C** Pourquoi et comment articuler industrialisation et lowtechisation *lownum/low15* `2h`
- **A** Vulgarisation *lownum/atelier-vulgarisation* `1h`
- **P** Vulgarisation & clôture `3-7h`
