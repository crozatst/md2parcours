# Lownum3 · Essaimage Lownum

Formation de formateur au cours lowtechisation et numérique

## Introduction et état de l'art

- **A** Lifephone *lownum/atelier-lifephone*`1h`
- **C** Introduction : présentation du cours, aperçu des concepts, méthodologie générale de projet *lownum/low00-essaimage* `1h`
- **P** Initialisation *methodo/initialisation-lownum-essaimage* `1h`
- **P** État de l'art *methodo/etat-de-l-art* `1h`

## Concepts et idéation

- **A** Scenarios Ademe *lownum/atelier-ademe* `1h`
- **C** Problématisation  : lowtechisation versus technosolutionnisme *lownum/low01* `1h`
- **C** Concepts de la lowtechisation : valeurs, leviers, tensions *lownum/low02a* `0h45`
- **C** Méthodologie générale pour lowtechiser et introduction à l'analyse fonctionnelle
 *lownum/low02b* `0h15`
- **P** Idéation *methodo/ideation* `1h`

## Maquettage et évaluation réflexive

- **A** Petites histoires *lownum/atelier-maquettage* `1h`
- **C** Évaluation qualitative réflexive a priori *lownum/low03* `1h`
- **A** Empreinte fantôme *lownum/atelier-evaluation* `1h`
- **P** Évaluation *methodo/evaluation* `1h`

## Publication et cours complémentaires (agir, impacts, communs, soin, besoins, juridique, industrie) 

- **P** Publication *methodo/publication* `1h`
- **C** Agir et prolonger : contribuer à la low-technicisation en tant qu'ingénieur *lownum/low21* `0h10`
- **C** De l’optimisation logicielle au permacomputing (focus sur la soutenabilité) *lownum/low06* `0h10`
- **C** Communs, culture libre et engagements collectifs (focus sur la responsabilité) *lownum/low14* `0h10`
- **C** Constitutivité technique et soin dans la technique (focus sur la convivialité) *lownum/low11* `0h10`
- **C** Besoins articiels, décroissance, fermeture *lownum/low12* `0h10`
- **C** Outils et enjeux juridiques et politiques `0h05`
- **C** Pourquoi et comment articuler industrialisation et lowtechisation `0h05`
- Revenir sur ou approfondir certains aspects du cours (préparation sur un pad) `1h`
- Revenir sur et approfondir (à partir du pad) ensemble `1h`

## Ateliers d'essaimage

- Atelier de réflexion sur ce qui peut-être fait en fonction des contextes de chacun (en sous-groupe) `1-2h`
- Présentation des projets `1-2h`
- Maquette de plans de formation en fonction de chacun `1-2h`

