CC BY SA https://stph.crzt.fr

# install

```
sudo apt get install pandoc
sudo apt get install xalan
```

# Doc

1. Manage Markdown content in data/ (original version of content)
2. Launch `./run.sh data/file.md`
3. Retrieve Scenari content in `out/file.prog`
4. Import in Scenari workspace *Parcours*
5. Redeploy program
